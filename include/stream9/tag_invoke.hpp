#ifndef STREAM9_TAG_INVOKE_HPP
#define STREAM9_TAG_INVOKE_HPP

#include <concepts>
#include <type_traits>
#include <utility>

namespace stream9 {

namespace tag_invoke_ {

    template <class T>
    typename std::decay_t<T>
    decay_copy(T&& v)
        noexcept(std::is_nothrow_convertible_v<T, std::decay_t<T>>)
    {
        return std::forward<T>(v);
    }

    void tag_invoke();

    struct api
    {
        template<typename Tag, typename... Args>
            requires requires (Tag t, Args&&... args) {
                tag_invoke((decay_copy)(t), std::forward<Args>(args)...);
            }
        decltype(auto)
        operator()(Tag t, Args&&... a) const
            noexcept (noexcept(tag_invoke((decay_copy)(t), std::forward<Args>(a)...)))
        {
            return tag_invoke((decay_copy)(t), std::forward<Args>(a)...);
        }
    };

} // namespace tag_invoke_

inline constexpr tag_invoke_::api tag_invoke;

template<auto& Tag>
using tag_t = std::decay_t<decltype(Tag)>;

template<typename Tag, typename... Args>
concept tag_invocable =
    std::invocable<decltype(tag_invoke), Tag, Args...>;

template<typename Tag, typename... Args>
concept nothrow_tag_invocable =
       tag_invocable<Tag, Args...>
    && std::is_nothrow_invocable_v<decltype(tag_invoke), Tag, Args...>;

template<typename Tag, typename... Args>
using tag_invoke_result =
        std::invoke_result<decltype(tag_invoke), Tag, Args...>;

template<typename Tag, typename... Args>
using tag_invoke_result_t =
        std::invoke_result_t<decltype(tag_invoke), Tag, Args...>;

} // namespace stream9

#endif // STREAM9_TAG_INVOKE_HPP
